package au.com.repository;

import au.com.domain.User;
import au.com.helper.TestHelper;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by amitsjoshi on 31/03/18.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
public class UserRepositoryTest extends TestCase
{
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testUserRetrievalById()
    {
        User user = TestHelper.createUser1();

        user = entityManager.persist(user);
        entityManager.flush();

        User userFromDb = userRepository.findOne(user.getId());

        assertEquals(user.getId(), userFromDb.getId());
        assertEquals(user.getUserName(), userFromDb.getUserName());

        entityManager.remove(user);
        assertEquals(1,1);
    }

}
